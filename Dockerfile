FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/helloworld-0.0.1-SNAPSHOT.jar
RUN echo ${JAR_FILE}
# cd /opt/app
WORKDIR /opt/app

# cp target/spring-boot-web.jar /opt/app/app.jar
COPY ${JAR_FILE} app.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]